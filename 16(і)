#include <iostream>

class Shape {
public:
    virtual void draw() const = 0; 
};

class Circle : public Shape {
private:
    double radius;

public:
    Circle(double radius) : radius(radius) {}

    void draw() const override {
        std::cout << "Drawing Circle with radius " << radius << "\n";
    }
};

class Rectangle : public Shape {
private:
    double width;
    double height;

public:
    Rectangle(double width, double height) : width(width), height(height) {}

    void draw() const override {
        std::cout << "Drawing Rectangle with width " << width << " and height " << height << "\n";
    }
};

class Triangle : public Shape {
private:
    double base;
    double height;

public:
    Triangle(double base, double height) : base(base), height(height) {}

    void draw() const override {
        std::cout << "Drawing Triangle with base " << base << " and height " << height << "\n";
    }
};

int main() {
    Circle circle(5.0);
    Rectangle rectangle(4.0, 3.0);
    Triangle triangle(6.0, 4.0);

    Shape* shapes[] = { &circle, &rectangle, &triangle };

    for (const auto shape : shapes) {
        shape->draw();
    }

    return 0;
}
