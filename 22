#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

struct Order {
    int orderNumber;
    std::string item;
    int quantity;

    Order(int orderNum, const std::string& itm, int qty)
        : orderNumber(orderNum), item(itm), quantity(qty) {}
};

class OrderManager {
private:
    std::vector<Order> orders;

public:
    void addOrder(int orderNumber, const std::string& item, int quantity) {
        orders.emplace_back(orderNumber, item, quantity);
        std::cout << "Order " << orderNumber << " for item \"" << item << "\" with quantity " << quantity << " added." << std::endl;
    }

    void removeOrderByNumber(int orderNumber) {
        auto it = std::remove_if(orders.begin(), orders.end(),
            [orderNumber](const Order& order) {
                return order.orderNumber == orderNumber;
            });
        if (it != orders.end()) {
            orders.erase(it, orders.end());
            std::cout << "Order " << orderNumber << " removed." << std::endl;
        }
        else {
            std::cout << "Order " << orderNumber << " not found." << std::endl;
        }
    }

    void removeOrderByItem(const std::string& item) {
        auto it = std::remove_if(orders.begin(), orders.end(),
            [item](const Order& order) {
                return order.item == item;
            });
        if (it != orders.end()) {
            orders.erase(it, orders.end());
            std::cout << "Orders for item \"" << item << "\" removed." << std::endl;
        }
        else {
            std::cout << "Orders for item \"" << item << "\" not found." << std::endl;
        }
    }

    void printAllOrders() const {
        if (orders.empty()) {
            std::cout << "No orders available." << std::endl;
            return;
        }
        std::cout << "All orders:" << std::endl;
        for (const auto& order : orders) {
            std::cout << "Order Number: " << order.orderNumber
                << ", Item: " << order.item
                << ", Quantity: " << order.quantity << std::endl;
        }
    }
};

int main() {
    OrderManager orderManager;
    orderManager.addOrder(1, "Laptop", 2);
    orderManager.addOrder(2, "Mouse", 10);
    orderManager.addOrder(3, "Keyboard", 5);

    orderManager.printAllOrders();

    orderManager.removeOrderByNumber(2);
    orderManager.printAllOrders();

    orderManager.removeOrderByItem("Keyboard");
    orderManager.printAllOrders();

    return 0;
}
